import re
import time
import json
from urllib.parse import quote
from flask import Flask
from flask import jsonify
from flask import request
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from pymongo import MongoClient
import requests

app = Flask(__name__)
cache = {
    'quran':{},
    'hadits':{}
}

with open("./resource/chapters-id.json", "r", encoding="utf-8") as read_file:
    Quran_chapters = json.load(read_file)

regex = re.compile('[^a-zA-Z]')

Quran_names = list(map(lambda x : regex.sub('', str(x['name_simple']).lower()), Quran_chapters['chapters']))

def get_quran(surat = 2, ayat = 255):

    cache_key = str(surat)+str(ayat)

    if cache_key in cache['quran']:
        return cache['quran'][cache_key]

    ayat_incr = 0

    for key, chapter in enumerate(Quran_chapters['chapters']):
    
        id_start = Quran_chapters['chapters'][key]['id_start'] = ayat_incr
        ayat_id = id_start + int(ayat)
        
        if int(chapter['chapter_number']) == int(surat):
            break

        ayat_incr = ayat_incr + chapter['verses_count']

    ep_path = 'https://quran.com/api/api/v3/chapters/{0}/verses/{1}'.format(str(surat), str(ayat_id))
    
    r = requests.get(
        ep_path,
        params={'language':'id', 'translations':'33'}
    )

    text = ''

    if r.status_code == 200:
        
        text += r.json()['verse']['text_madani']+"\n\n"

        for translation in r.json()['verse']['translations']:
            if translation['resource_id'] == 33:

                info = 'Surat {0} ayat {1} juz {2} halaman {3}'.format(
                    chapter['name_simple'],
                    ayat,
                    r.json()['verse']['juz_number'],
                    r.json()['verse']['page_number']
                )

                text += translation['text']+"\n\n"
                text += info+"\n"

                cache['quran'][cache_key] = text
                break
    else:
        text = '''
Mohon maaf terdapat kesalahan sistem untuk mendapatkan data, mohon coba beberapa saat lagi.
Ketik *bantuan* untuk petunjuk penggunaan.
'''
    
    return text


def get_hadits(imam = 'bukhari', nomor = 1):
    
    cache_key = imam+str(nomor)

    if cache_key in cache['hadits']:
        return cache['hadits'][cache_key]
    
    ts = str(int(time.time()))
    headers = {
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
        'Cookie': 'ARRAffinity=571d1a10224b33997946a390c370bd7ca8df99a1d389f093e06586de4724fc2e; visit=1',
        'Origin': 'http://hadits.in',
        'Referer': 'http://hadits.in/?'+imam+'/'+str(nomor),
        'X-Requested-With': 'XMLHttpRequest'
    }

    r = requests.post(
        'http://hadits.in/viewer/service_eh2.php',
        params={'timeStamp':ts},
        headers=headers,
        json={
            'jsonrpc': '2.0',
            'method': 'getHadits',
            'params': [
                imam,
                nomor
            ],
            'id': nomor
        }
    )

    if r.status_code == 200 and r.json()['result'] != 'tidak ditemukan':

        text = str(r.json()['result']['Isi_Arab'])+"\n\n"
        text += r.json()['result']['Isi_Indonesia']+"\n\n"
        text += 'HR '+r.json()['result']['imam']+':'+str(r.json()['result']['NoHdt'])

        cache['hadits'][cache_key] = text
        return text
    
    text = '''
Mohon maaf terdapat kesalahan sistem untuk mendapatkan data, mohon coba beberapa saat lagi.
Ketik *bantuan* untuk petunjuk penggunaan.
'''

def quran_parser(message_in, msg_arr):

    msg_arr[1] = msg_arr[1].replace('-', '')

    if msg_arr[1].isdigit() and msg_arr[2].isdigit():
        return get_quran(
            msg_arr[1],
            msg_arr[2]
        )
    
    if msg_arr[1].isalpha() and msg_arr[-1].isdigit():
        
        param_name = message_in.replace(msg_arr[0], '').replace(msg_arr[-1], '').replace(' ', '')
        score = process.extractOne(regex.sub('', param_name), Quran_names)
        surah_name = score[0]
        surah_id = Quran_names.index(surah_name) + 1

        return get_quran(
            surah_id,
            msg_arr[-1]
        )

def hadits_parser(message_in, msg_arr):
    
    if msg_arr[1].isalpha() and msg_arr[2].isdigit():

        score = process.extractOne(msg_arr[1], [
            'muslim',
            'ahmad',
            'bukhari',
            'abudaud',
            'tirmidzi',
            'nasai',
            'ibnumajah',
            'malik',
            'darimi'
        ])

        return get_hadits(
            score[0],
            msg_arr[2]
        )

def parser(message_in):
    
    default_message = '''
Mohon maaf Thalib belum paham maksud kamu. Ketik *bantuan* untuk mengetahui cara penggunaan.
    '''
    
    message_in = message_in.lower()
    msg_arr = message_in.split()
    score = process.extractOne(msg_arr[0], [
        'quran', 'hadits',
        'surat', 'help', 'bantuan', 'tolong',
        'السلام عليكم', 'asalamualaikum',
        'hukum',
        'جزاك اللهُ', 'jazakallah'
    ])

    if score[0] in ['السلام عليكم', 'asalamualaikum'] and score[1] > 70:
        return 'وعليكم السلام ورحمة الله وبركاته'
    
    if score[0] in ['جزاك اللهُ', 'jazakallah'] and score[1] > 70:
        return 'جَزَاكُمُ اللهُ خيرًا'
    
    if score[0] in ['quran', 'surat'] and len(msg_arr) >= 3:
        return quran_parser(message_in, msg_arr)
    
    if score[0] == 'hadits' and len(msg_arr) is 3:
        return hadits_parser(message_in, msg_arr)
    
    if score[0] == 'hukum' and score[1] > 90 and len(msg_arr) > 1:
        return '''
Berikut pencarian tentang {0} di layanan konsultasisyariah.com

https://www.google.co.id/search?q=site%3Akonsultasisyariah.com&q={1}
        '''.format(message_in, quote(message_in, safe=''))
    
    if score[0] in ['help', 'bantuan', 'tolong'] and score[1] > 90:
        return '''
Untuk mendapatkan ayat Al Quran formatnya:

*quran NOMOR_SURAT NOMOR_AYAT* atau
*quran NAMA_SURAT NOMOR_AYAT*

Contoh:
*quran 2 255*
*quran albaqarah 255*

Untuk mendapatkan data hadits formatnya
*hadits NAMA_IMAM NOMOR_HADITS*

Contoh:
*hadits muslim 2*

Dapatkan pahala jariyah dari setiap orang yang merasakan manfaat dari layanan ini. Sebarkan nomor ini +6283872082835 ke semua saudara-saudara kita.
        '''    
    return default_message

@app.route("/")
def hello():
    return 'السلام عليكم'

@app.route("/v1/wa", methods=['POST'])
def v1_wa_post():

    if 'number' in request.form:
        if len(request.form['number'].split('@')) > 1:
            return ''
    
    return jsonify({
        'message-out':parser(request.form['message-in']),
        'delay':0
    })

if __name__ == "__main__":
    app.run(debug=True)